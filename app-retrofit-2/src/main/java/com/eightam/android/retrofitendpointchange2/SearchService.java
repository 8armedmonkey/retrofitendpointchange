package com.eightam.android.retrofitendpointchange2;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface SearchService {

    @GET("/search")
    Observable<Response<ResponseBody>> search(@Query("q") String query);

}
