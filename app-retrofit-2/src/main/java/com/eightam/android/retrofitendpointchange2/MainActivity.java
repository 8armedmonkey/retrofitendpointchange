package com.eightam.android.retrofitendpointchange2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Proof of concept changing the Retrofit endpoint / base URL during runtime.
 * Try to use different hosts (for Retrofit 2.x without the scheme, e.g: https):
 * www.google.com, www.yahoo.com, www.bing.com, etc
 */
public class MainActivity extends AppCompatActivity {

    private static final String DEFAULT_HOST = "www.google.com";

    private HttpUrl httpUrl;
    private HostSelectionInterceptor hostSelectionInterceptor;
    private SearchService searchService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        initializeViews();
    }

    private void initialize() {
        httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host(DEFAULT_HOST)
                .build();

        hostSelectionInterceptor = new HostSelectionInterceptor();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(hostSelectionInterceptor)
                .build();

        searchService = new Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(SearchService.class);
    }

    private void initializeViews() {
        Button buttonMakeRequest = (Button) findViewById(R.id.button_make_request);
        EditText editEndpoint = (EditText) findViewById(R.id.edit_endpoint);

        if (buttonMakeRequest != null) {
            buttonMakeRequest.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    EditText editEndpoint = (EditText) findViewById(R.id.edit_endpoint);

                    if (editEndpoint != null) {
                        String endpointUrl = editEndpoint.getText().toString();
                        httpUrl = httpUrl.newBuilder().host(endpointUrl).build();
                        hostSelectionInterceptor.setHost(endpointUrl);
                    }

                    search();
                }

            });
        }

        if (editEndpoint != null) {
            editEndpoint.setText(DEFAULT_HOST);
        }
    }

    private void search() {
        searchService.search("test")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(displayResponseAction(), printStackTraceAction());
    }

    private Action1<Response<ResponseBody>> displayResponseAction() {
        return new Action1<Response<ResponseBody>>() {

            @Override
            public void call(Response<ResponseBody> response) {
                displayResponse(response);
            }

        };
    }

    private Action1<Throwable> printStackTraceAction() {
        return new Action1<Throwable>() {

            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }

        };
    }

    private void displayResponse(Response<ResponseBody> response) {
        TextView textResponse = (TextView) findViewById(R.id.text_response);

        if (textResponse != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("URL: ").append(response.raw().request().url()).append("\n");
            sb.append("Status: ").append(response.code()).append("\n");
            sb.append("Body: ").append("\n");

            InputStream inputStream = null;

            try {
                inputStream = response.body().byteStream();
                sb.append(IOUtils.toString(inputStream)).append("\n");
            } catch (Throwable t) {
                t.printStackTrace();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }

            textResponse.setText(sb.toString());

        }
    }

}
