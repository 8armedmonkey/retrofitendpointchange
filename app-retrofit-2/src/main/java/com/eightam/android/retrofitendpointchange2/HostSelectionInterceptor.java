package com.eightam.android.retrofitendpointchange2;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HostSelectionInterceptor implements Interceptor {

    private String host;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (host != null) {
            HttpUrl url = request.url().newBuilder()
                    .host(host)
                    .build();

            request = request.newBuilder()
                    .url(url)
                    .build();
        }
        return chain.proceed(request);
    }

    public void setHost(String host) {
        this.host = host;
    }

}
