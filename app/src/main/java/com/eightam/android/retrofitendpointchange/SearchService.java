package com.eightam.android.retrofitendpointchange;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface SearchService {

    @GET("/search")
    Observable<Response> search(@Query("q") String query);

}
