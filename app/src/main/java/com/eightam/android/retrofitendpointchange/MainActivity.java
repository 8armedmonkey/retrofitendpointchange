package com.eightam.android.retrofitendpointchange;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;

import retrofit.RestAdapter;
import retrofit.client.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Proof of concept changing the Retrofit endpoint / base URL during runtime.
 * Try to use different hosts (for Retrofit 2.x without the scheme, e.g: https):
 * www.google.com, www.yahoo.com, www.bing.com, etc
 */
public class MainActivity extends AppCompatActivity {

    private MutableEndpoint endpoint;
    private SearchService searchService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        initializeViews();
    }

    private void initialize() {
        endpoint = new MutableEndpoint("https://www.google.com", "search");
        searchService = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(SearchService.class);
    }

    private void initializeViews() {
        Button buttonMakeRequest = (Button) findViewById(R.id.button_make_request);
        EditText editEndpoint = (EditText) findViewById(R.id.edit_endpoint);

        if (buttonMakeRequest != null) {
            buttonMakeRequest.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    EditText editEndpoint = (EditText) findViewById(R.id.edit_endpoint);

                    if (editEndpoint != null) {
                        String endpointUrl = editEndpoint.getText().toString();
                        endpoint.setUrl(endpointUrl);
                    }

                    search();
                }

            });
        }

        if (editEndpoint != null) {
            editEndpoint.setText(endpoint.getUrl());
        }
    }

    private void search() {
        searchService.search("test")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(displayResponseAction(), printStackTraceAction());
    }

    private Action1<Response> displayResponseAction() {
        return new Action1<Response>() {

            @Override
            public void call(Response response) {
                displayResponse(response);
            }

        };
    }

    private Action1<Throwable> printStackTraceAction() {
        return new Action1<Throwable>() {

            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }

        };
    }

    private void displayResponse(Response response) {
        TextView textResponse = (TextView) findViewById(R.id.text_response);

        if (textResponse != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("URL: ").append(response.getUrl()).append("\n");
            sb.append("Status: ").append(response.getStatus()).append("\n");
            sb.append("Body: ").append("\n");

            InputStream inputStream = null;

            try {
                inputStream = response.getBody().in();
                sb.append(IOUtils.toString(inputStream)).append("\n");
            } catch (Throwable t) {
                t.printStackTrace();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }

            textResponse.setText(sb.toString());

        }
    }

}
