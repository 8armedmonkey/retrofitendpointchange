package com.eightam.android.retrofitendpointchange;

import retrofit.Endpoint;

public class MutableEndpoint implements Endpoint {

    private String url;
    private String name;

    public MutableEndpoint(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
